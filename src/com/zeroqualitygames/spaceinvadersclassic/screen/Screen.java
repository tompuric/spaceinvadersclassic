package com.zeroqualitygames.spaceinvadersclassic.screen;

import java.awt.image.BufferedImage;


public class Screen {
	public int[] pixels;
	public int width;
	public int height;
	public final int YELLOW = -256;

	public static BufferedImage image;

	public Screen(int width, int height, BufferedImage image) {
		this.width = width;
		this.height = height;
		Screen.image = image;
		
		pixels = new int[width * height];
	}
	
	public void reset(int color) {
		for (int i = 0; i < pixels.length; i++) {
			pixels[i] = color;
		}
	}
	
	public void reset(int i, int color) {
		pixels[i] = color;
	}
	
	private boolean transparant(int pixel) {
		return ((pixel << 16 & 0xFF) == 0) && ((pixel << 8 & 0xFF) == 0) && ((pixel & 0xFF) == 0);
	}
	
	public void render(int x, int y, int tile, int color, int width, int height) {

		int position = x + (y * this.width);
		int xt = tile%27 * 8;
		int yt = tile/27 * 8;

		for (int a = yt; a < yt + height; a++) {
			for (int b = xt; b < xt + width; b++) {
				int pixel = image.getRGB(b, a);
				if (transparant(pixel)) {
					position++;
					continue;
				}
				pixel = color;
				pixels[position++] = pixel;

			}
			position += this.width - width;
		}
	}
	
	public void render(int x, int y, int color, int width, int height, int[] imagePixels) {

		int position = x + (y * this.width);
		for (int i = 0; i < imagePixels.length; i++) {
			int pixel = imagePixels[i];
			if (i % width == 0 & i != 0) {
				position += this.width - width;
			}
			if (transparant(pixel)) {
				position++;
				continue;
			}
			pixel = color;
			pixels[position++] = pixel;
		}
	}
	
	public static int[] getImage(int tile, int width, int height) {
		int xt = tile%27 * 8;
		int yt = tile/27 * 8;
		int[] pixels = new int[width * height];
		int position = 0;
		for (int a = yt; a < yt + height; a++) {
			for (int b = xt; b < xt + width; b++) {
				pixels[position++] = image.getRGB(b, a);
			}
		}
		return pixels;
	}
	
	/*			
	  			System.out.println("A = " + ((pixel >> 24) & 0x000000FF));
				System.out.println("R = " + ((pixel >> 16) & 0x000000FF));
				System.out.println("G = " + ((pixel >> 8) & 0x000000FF));
				System.out.println("B = " + ((pixel) & 0x000000FF));
				System.out.println();
				*/

}
