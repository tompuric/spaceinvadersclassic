package com.zeroqualitygames.spaceinvadersclassic;

import java.awt.BorderLayout;

import javax.swing.JFrame;



public class GameMain extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Game game;

	public GameMain(String title) {
		super(title);
		game = new Game();
		
		setSize(Game.WIDTH, Game.HEIGHT);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setIgnoreRepaint(true);
		
		setLocationRelativeTo(null);
		//setResizable(false);
		setVisible(true);
		
		
		setLayout(new BorderLayout());
		add(game, BorderLayout.CENTER);
		
		pack();
		game.start();
	}
	
	public static void main(String[] args) {
		new GameMain("Space Invaders Classic by Zero Quality Games");
	}


}
