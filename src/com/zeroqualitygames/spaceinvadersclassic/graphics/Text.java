package com.zeroqualitygames.spaceinvadersclassic.graphics;

import com.zeroqualitygames.spaceinvadersclassic.screen.Screen;

public class Text {
	
	public static String characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ " + 
						"0123456789.,!?'\"-+=/\\%()<>:";


	public Text() {
		// TODO Auto-generated constructor stub
	}
	
	public static void drawString(Screen screen, String message, int x, int y, int color) {
		message = message.toUpperCase();
		for (int i = 0; i < message.length(); i++) {
			int tile = characters.indexOf(message.charAt(i));
			screen.render(x + (i * 8), y, tile, color, 8, 8);
			
		}
	}

}
