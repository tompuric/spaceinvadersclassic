package com.zeroqualitygames.spaceinvadersclassic.graphics;

public class Color {

	public Color() {
		// TODO Auto-generated constructor stub
	}
	
	public static int get(int r, int g, int b) {
		return r << 16 | g << 8 | b;
	}
	
	public static int get(int color) {
		int r = (color >> 16 & 0x000000FF);
		int g = (color >> 8 & 0x000000FF);
		int b = (color & 0x000000FF);
		return r << 16 | g << 8 | b;
	}

}


/*
int r1 = r << 16;
int g1 = g << 8;
int b1 = b;
System.out.println("r = \t" + r1 + "\tg = \t" + g1 + "\tb = \t" + b1);
System.out.println(r << 16 | g << 8 | b);
*/