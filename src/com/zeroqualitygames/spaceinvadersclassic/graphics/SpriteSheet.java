package com.zeroqualitygames.spaceinvadersclassic.graphics;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URL;

import javax.imageio.ImageIO;

public class SpriteSheet {
	public BufferedImage image;
	public int[] pixels;

	public SpriteSheet(String name) {
		URL url = this.getClass().getResource(name);
		try {
			image = ImageIO.read(url);
		} catch (IOException e) {
			System.out.println("Trouble loading the image: " + name);
			e.printStackTrace();
		}
		pixels = image.getRGB(0, 0, image.getWidth(), image.getHeight(), null, 0, image.getWidth());
	}

}
