package com.zeroqualitygames.spaceinvadersclassic;

import java.awt.Canvas;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;

import com.zeroqualitygames.spaceinvadersclassic.InputManager;
import com.zeroqualitygames.spaceinvadersclassic.graphics.SpriteSheet;
import com.zeroqualitygames.spaceinvadersclassic.screen.Screen;
import com.zeroqualitygames.spaceinvadersclassic.world.World;

public class Game extends Canvas implements Runnable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final int WIDTH = 200;
	public static final int HEIGHT = 200;
	
	private BufferedImage image = new BufferedImage(WIDTH, HEIGHT, BufferedImage.TYPE_INT_RGB);
	private int[] pixels = ((DataBufferInt) image.getRaster().getDataBuffer()).getData();
	// image.getRaster() -> returns the Raster (rectangular array of pixels) object of image which is Writable
	// .getDataBuffer() -> returns a DataBuffer representing the Raster
	// ((DataBufferInt) object).getData() returns the array of integers representing the colour of each pixel
	private int[] colors = new int[256];
	
	private Thread gameThread;
	private boolean running = false;
	
	private InputManager input = new InputManager(this);
	
	private SpriteSheet tileSet;
	private Screen screen;
	private World world = new World(this, input);
	
	private int ns = 1000000000; // 1 000 000 000
	public int FPS;
	
	private int tps = 60;
	private int ticks = 0;
	private int renders = 0;
	@SuppressWarnings("unused")
	private int value = 0;

	public long beforeTime = System.nanoTime();
	public long afterTime;
	public long difference;

	public Game() {
		setPreferredSize(new Dimension(500, 500));
		requestFocus();
		init();
	}
	
	public void init() {
		int count = 0;
		for (int a = 0; a < 4; a++) {
			for (int r = 0; r < 4; r++) {
				for (int g = 0; g < 4; g++) {
					for (int b = 0; b < 4; b++) {
						colors[count++] = 0xFF << 24 | r * 0xAA << 16 | g * 0xAA << 8 | b * 0xAA;
						// Only 64 colours
					}
				}
			}
		}
		tileSet = new SpriteSheet("spaceinvaderTileSet.png");
		screen = new Screen(WIDTH, HEIGHT, tileSet.image);
		world.init();
	}

	@Override
	public void run() {
		int sleepTime = 2;
		
		long startTime = System.nanoTime();
		long start = System.nanoTime();
		double nsPerTick = ns/tps;
		
		while (running) {
			double now = (System.nanoTime() - start);

			
			if (now > nsPerTick) {
				update();
				start = System.nanoTime();
			}
			
			try {
				Thread.sleep(sleepTime);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
			render();
			
			
			if (System.nanoTime() - startTime > 1*ns) {
				System.out.println("tps: " + ticks + "\tfps: " + renders);
				FPS = renders;
				ticks = 0;
				renders = 0;
				value++;
				startTime = System.nanoTime();
			}
		}
	}
	
	public void update() {
		ticks++;
		world.tick();
	}
	
	public void render() {
		renders++;
		BufferStrategy strategy = getBufferStrategy();
		if (strategy == null) {
			createBufferStrategy(3);
			requestFocus();
			return;
		}
		
		paintComponent(screen);
		
		Graphics g = strategy.getDrawGraphics();
		g.fillRect(0, 0, getWidth(), getHeight());
		g.drawImage(image, (getWidth() - getHeight())/2, 0, getHeight(), getHeight(), this);
		g.dispose();
		strategy.show();
	}
	

	public void paintComponent(Screen screen) {
		for (int i = 0; i < pixels.length; i++) {
			screen.reset(i, colors[0]);
		}
		
		world.render(screen);
		
		for (int i = 0; i < pixels.length; i++) {
			if (screen.pixels[i] != 0)
				pixels[i] = screen.pixels[i];
		}
		
	}
	
	public void start() {
		if (!running && gameThread == null) {
			running = true;
			gameThread = new Thread(this);
			gameThread.start();
		}
	}
	
	public void end() {
		running = !running;
	}

}
