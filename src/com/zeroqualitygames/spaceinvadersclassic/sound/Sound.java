package com.zeroqualitygames.spaceinvadersclassic.sound;

import java.applet.Applet;
import java.applet.AudioClip;
import java.net.URL;

public class Sound {
	
	public static Sound shoot = new Sound("shoot.wav");
	public static Sound killed = new Sound("invaderkilled.wav");

	private AudioClip clip;
	
	public Sound(String name) {
		URL url = this.getClass().getResource(name);
		clip = Applet.newAudioClip(url);
	}
	
	public void play() {
		try {
			new Thread(new Runnable() {

				@Override
				public void run() {
					clip.play();
				}
				
			}).start();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void stop() {
		try {
			new Thread(new Runnable() {

				@Override
				public void run() {
					clip.stop();
				}
				
			}).start();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void loop() {
		try {
			new Thread(new Runnable() {

				@Override
				public void run() {
					clip.loop();
				}
				
			}).start();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

}
