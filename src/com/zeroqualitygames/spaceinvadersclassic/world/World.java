package com.zeroqualitygames.spaceinvadersclassic.world;

import java.awt.Rectangle;
import java.util.ArrayList;

import com.zeroqualitygames.spaceinvadersclassic.Game;
import com.zeroqualitygames.spaceinvadersclassic.InputManager;
import com.zeroqualitygames.spaceinvadersclassic.entity.Enemy;
import com.zeroqualitygames.spaceinvadersclassic.entity.Entity;
import com.zeroqualitygames.spaceinvadersclassic.entity.LargeAlien;
import com.zeroqualitygames.spaceinvadersclassic.entity.MediumAlien;
import com.zeroqualitygames.spaceinvadersclassic.entity.Player;
import com.zeroqualitygames.spaceinvadersclassic.entity.SmallAlien;
import com.zeroqualitygames.spaceinvadersclassic.entity.Wall;
import com.zeroqualitygames.spaceinvadersclassic.graphics.Color;
import com.zeroqualitygames.spaceinvadersclassic.graphics.Text;
import com.zeroqualitygames.spaceinvadersclassic.screen.Screen;

public class World {
	public Game game;
	public InputManager input;
	public ArrayList<Entity> entities = new ArrayList<Entity>();
	public Player player;
	public int[] pixels;
	public long dropTime = System.currentTimeMillis();
	public long moveTime = System.currentTimeMillis();
	private boolean aliensReversed = false;

	public World(Game game, InputManager input) {
		this.game = game;
		this.input = input;
		player = new Player(input, Game.WIDTH/2, Game.HEIGHT - 8);
		reset();
	}
	
	public void init() {
		for (Entity e : entities)
			e.init(this);
	}
	
	public void reset() {
		int xSpace = 16;
		int ySpace = 10;
		
		ArrayList<Entity> e = entities;
		for (int i = 0; i < e.size(); i++) {
			removeEntity(e.get(i));
		}
		
		
		entities.add(player);
		entities.add(new Wall(40));
		entities.add(new Wall(90));
		entities.add(new Wall(140));
		
		for (int x = 0; x < 11; x++) {
			entities.add(new SmallAlien(5 + x * xSpace, 20));
		}
		
		for (int y = 0; y < 2; y++) {
			for (int x = 0; x < 11; x++) {
				entities.add(new MediumAlien(x * xSpace, 35 + y * ySpace));
			}
		}
		
		for (int y = 0; y < 2; y++) {
			for (int x = 0; x < 11; x++) {
				entities.add(new LargeAlien(x * xSpace, 60 + y * ySpace));
			}
		}
	}
	
	public void tick() {
		
		ArrayList<Entity> e = entities;
		for (int i = 0; i < e.size(); i++) {
			e.get(i).tick();
		}
		
		for (int i = 0; i < e.size(); i++) {
			if (e.get(i).removed)
				removeEntity(e.get(i));
		}

		for (int i = 0; i < e.size(); i++) {
			int count = 0;
			if (e.get(i) instanceof Enemy) {
				count++;
				if (e.get(i).x + e.get(i).width + e.get(i).vx > Game.WIDTH || e.get(i).x + e.get(i).vx < 0) {
					aliensReversed = true;
				}
				if (count == 0)
					reset();
			}
		}
		
		if (aliensReversed) {
			for (int i = 0; i < e.size(); i++) {
				if (e.get(i) instanceof Enemy)
					e.get(i).reverse();
			}
			aliensReversed = false;
		}
		
		if (System.currentTimeMillis() - moveTime > 1000) {
			for (int i = 0; i < e.size(); i++) {
				if (e.get(i) instanceof Enemy) {
					e.get(i).move(e.get(i).vx, 0);
				}
			}
			moveTime = System.currentTimeMillis();
		}
		
		if (System.currentTimeMillis() - dropTime > 2000) {
			for (int i = 0; i < e.size(); i++) {
				if (e.get(i) instanceof Enemy)
					e.get(i).drop();
			}
			dropTime = System.currentTimeMillis();
		}
	}
	
	public void removeEntity(Entity e) {
		entities.remove(e);
	}
	
	public void addEntity(Entity e) {
		entities.add(e);
	}
	
	public ArrayList<Entity> getEntities(Rectangle rect) {
		ArrayList<Entity> e = new ArrayList<Entity>();
		for (int i = 0; i < entities.size(); i++) {
			Entity ent = entities.get(i);
			if (ent.rect.equals(rect))
				continue;
			else {
				if (ent.rect.intersects(rect)) {
					e.add(ent);
				}
			}
		}
		return e;
	}
	
	public void render(Screen screen) {
		for (Entity e : entities)
			e.render(screen);
				
		Text.drawString(screen, "SCORE: " + player.score + " HP: " + player.lives/2 + " FPS: " + game.FPS, 0, 0, Color.get(255, 255, 255));
	}

}
