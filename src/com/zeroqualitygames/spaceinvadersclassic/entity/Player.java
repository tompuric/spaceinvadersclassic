package com.zeroqualitygames.spaceinvadersclassic.entity;

import java.awt.Rectangle;

import com.zeroqualitygames.spaceinvadersclassic.graphics.Color;
import com.zeroqualitygames.spaceinvadersclassic.screen.Screen;
import com.zeroqualitygames.spaceinvadersclassic.sound.Sound;
import com.zeroqualitygames.spaceinvadersclassic.Game;
import com.zeroqualitygames.spaceinvadersclassic.InputManager;

public class Player extends Entity {
	public InputManager input;
	public int xTile = 3;
	public int yTile = 2;
	
	public int score;
	public int lives = 6;
	
	public int tileSize = 8;
	public int color = Color.get(255, 255, 0);
	public long previousShootTime = 0;
	
	public final int LEFT = -1;
	public final int RIGHT = 1;
	public final int STILL = 0;

	public Player(InputManager input, int x, int y) {
		super("Player");
		this.input = input;
		this.x = x;
		this.y = y;
		width = 13;
		height = 8;
		score = 0;
		rect = new Rectangle(x, y, width, height);
	}
	
	public void tick() {
		input.tick();
		vx = STILL;
		
		if (input.left.pressed) {
			vx = LEFT;
		}
		if (input.right.pressed) {
			vx = RIGHT;
		}
		
		if (input.shoot.pressed) {
			if (System.currentTimeMillis() - previousShootTime > 400) {
				Missile m = new Missile(world, x + width/2, y - 3, -2, Color.get(0, 255, 0), name);
				Sound.shoot.play();
				world.addEntity(m);
				previousShootTime = System.currentTimeMillis();
			}
		}
		
		if (x + width + vx > Game.WIDTH)
			return;
		if (x + vx < 0)
			return;
		
		if (lives < 0)
			removed = true;
		
		move(vx, 0);
	}
	
	public void render(Screen screen) {
		screen.render(x, y, 9 + (2 * 27), color, width, 8);
	}
	
	public boolean touchedBy(Entity e) {
		if (e instanceof Missile) {
			if (!e.name.equals(name)) {
				lives--;
				e.remove();
				return true;
			}
			
		}
		return false;
	}
}
