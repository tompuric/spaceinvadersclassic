package com.zeroqualitygames.spaceinvadersclassic.entity;

import java.awt.Rectangle;

import com.zeroqualitygames.spaceinvadersclassic.screen.Screen;
import com.zeroqualitygames.spaceinvadersclassic.world.World;
import com.zeroqualitygames.spaceinvadersclassic.Game;

public class Missile extends Entity {
	
	
	private int color;

	public Missile(World world, int x, int y, int vy, int color, String name) {
		super(name);
		this.world = world;
		this.x = x;
		this.y = y;
		this.color = color;
		this.vy = vy;
		width = 1;
		height = 3;
		rect = new Rectangle(x, y, width, height);
	}
	
	public void tick() {
		if (y + vy < 0) {
			removed = true;
		}
		if (y + vy >= Game.HEIGHT - 2) {
			removed = true;
			return;
		}
		move(0, vy);
	}
	
	public void render(Screen screen) {
		screen.render(x, y, 2 + (1*25), color, width, height);
	}
	
	public boolean touchedBy(Entity entity) {
		if (!(entity instanceof Missile)) {
			entity.touchedBy(this);
			return true;
		}
		return false;
	}

}
