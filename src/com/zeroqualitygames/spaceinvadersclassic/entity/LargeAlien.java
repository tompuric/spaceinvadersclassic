package com.zeroqualitygames.spaceinvadersclassic.entity;

import java.awt.Rectangle;

import com.zeroqualitygames.spaceinvadersclassic.graphics.Color;
import com.zeroqualitygames.spaceinvadersclassic.screen.Screen;

public class LargeAlien extends Enemy {

	public LargeAlien(int x, int y) {
		super("LargeAlien");
		this.x = x;
		this.y = y;
		points = 10;
		tile = 7 + (2 * 27);
		width = 16;
		height = 8;
		rect = new Rectangle(x, y, width, height);
	}
	
	public void tick() {
		super.tick();
	}
	
	public boolean touchedBy(Entity e) {
		super.touchedBy(e);
		return false;
	}
	
	public void render(Screen screen) {
		screen.render(x, y, tile, Color.get(255, 255, 255), width, height);
	}

}
