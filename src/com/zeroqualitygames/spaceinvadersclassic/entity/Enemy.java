package com.zeroqualitygames.spaceinvadersclassic.entity;

import java.util.Random;

import com.zeroqualitygames.spaceinvadersclassic.graphics.Color;
import com.zeroqualitygames.spaceinvadersclassic.screen.Screen;
import com.zeroqualitygames.spaceinvadersclassic.sound.Sound;

public abstract class Enemy extends Entity {
	public Random random = new Random();
	public int points;
	private long lastReloadTime = System.currentTimeMillis();
	private int reloadTime;
	
	public Enemy(String name) {
		super(name);
		reloadTime = 1000 + random.nextInt(30000);
		System.out.println(reloadTime);
		vx = 1;
	}
	
	public void tick() {
		if (System.currentTimeMillis() - lastReloadTime > reloadTime) {
			world.addEntity(new Missile(world, x + random.nextInt(width), y + height, 1, Color.get(255, 255, 255), name));
			lastReloadTime = System.currentTimeMillis();
		}
	}
	
	public void move(int vx, int vy) {
		super.move(vx, vy);
	}

	public void render(Screen screen) {
		
	}
	
	public boolean touchedBy(Entity e) {
		if (e instanceof Missile) {
			if (e.name.equals("Player")) {
				world.player.score += points;
				Sound.killed.play();
				super.remove();
				e.remove();
				return true;
			}
		}
		return false;
	}
	
	public void reverse() {
		vx = -vx;
	}
	
	public void drop() {
		y += 2;
		rect.setLocation(x, y);
	}

}
