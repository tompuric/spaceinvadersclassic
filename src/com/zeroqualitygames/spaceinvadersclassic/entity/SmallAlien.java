package com.zeroqualitygames.spaceinvadersclassic.entity;

import java.awt.Rectangle;

import com.zeroqualitygames.spaceinvadersclassic.graphics.Color;
import com.zeroqualitygames.spaceinvadersclassic.screen.Screen;

public class SmallAlien extends Enemy {

	public SmallAlien(int x, int y) {
		super("SmallAlien");
		this.x = x;
		this.y = y;
		points = 30;
		tile = 0 + (2 * 27);
		width = 8;
		height = 8;
		rect = new Rectangle(x, y, width, height);
	}
	
	public void tick() {
		super.tick();
	}
	
	public boolean touchedBy(Entity e) {
		super.touchedBy(e);
		return false;
	}
	
	public void render(Screen screen) {
		screen.render(x, y, tile, Color.get(255, 255, 255), width, height);
	}
}
