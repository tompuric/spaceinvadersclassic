package com.zeroqualitygames.spaceinvadersclassic.entity;

import java.awt.Rectangle;

import com.zeroqualitygames.spaceinvadersclassic.graphics.Color;
import com.zeroqualitygames.spaceinvadersclassic.screen.Screen;
import com.zeroqualitygames.spaceinvadersclassic.world.World;
import com.zeroqualitygames.spaceinvadersclassic.Game;

public class Wall extends Entity {
	int[] pixels;
	int color = Color.get(0, 255, 0);

	public Wall(int x) {
		super("Wall");
		this.x = x;
		this.y = Game.HEIGHT - 35;
		tile = 11 + (2 * 27);
		width = 20;
		height = 15;
		rect = new Rectangle(x, y, width, height);
		pixels = new int[width*height];
		
	}
	
	public void init(World world) {
		this.world = world;
		pixels = Screen.getImage(tile, width, height);
	}
	
	public void render(Screen screen) {
		screen.render(x, y, color, width, height, pixels);
	}
	
	public boolean touchedBy(Entity entity) {
		if (entity instanceof Missile) {
			if (chip(entity.x, entity.vy)) {
				entity.remove();
				return true;
			}
		}
		return false;
	}
	
	private boolean chip(int x, int vy) {
		if (vy > 0) {
			for (int i = 0; i < pixels.length; i++) {
				if (i % width  == x - this.x) {
					if (Color.get(pixels[i]) == 0)
						continue;
					pixels[i] = Color.get(0, 0, 0);
					try {
						pixels[i + width] = Color.get(0, 0, 0);
						pixels[i + width*2] = Color.get(0, 0, 0);
					} catch (Exception e){}
					return true;
				}
			}
			return false;
		}
		else {
			for (int i = pixels.length - 1; i >= 0; i--) {
				if (i % width  == x - this.x) {
					if (Color.get(pixels[i]) == 0)
						continue;
					pixels[i] = Color.get(0, 0, 0);
					try {
						pixels[i - width] = Color.get(0, 0, 0);
						pixels[i - width*2] = Color.get(0, 0, 0);
					} catch (Exception e){}
					return true;
				}
			}
			return false;
		}

	}

}
