package com.zeroqualitygames.spaceinvadersclassic.entity;

import java.awt.Rectangle;

import com.zeroqualitygames.spaceinvadersclassic.graphics.Color;
import com.zeroqualitygames.spaceinvadersclassic.screen.Screen;

public class MotherAlien extends Enemy {
	
	public MotherAlien(int x, int y) {
		super("MotherAlien");
		this.x = x;
		this.y = y;
		points = 100;
		tile = 3 + (2 * 27);
		width = 16;
		height = 8;
		rect = new Rectangle(x, y, width, height);
	}
	
	public void tick() {
		//super.tick();
	}
	
	public boolean touchedBy(Entity e) {
		super.touchedBy(e);
		return false;
	}
	
	public void render(Screen screen) {
		screen.render(x, y, tile, Color.get(255, 0, 0), width, height);
	}
}
