package com.zeroqualitygames.spaceinvadersclassic.entity;

import java.awt.Rectangle;
import java.util.ArrayList;

import com.zeroqualitygames.spaceinvadersclassic.screen.Screen;
import com.zeroqualitygames.spaceinvadersclassic.world.World;

public class Entity {
	
	public World world;
	public String name;
	public Rectangle rect;
	public int x;
	public int y;
	public int width;
	public int height;
	public int vx;
	public int vy;
	public int tile;
	
	public boolean removed = false;

	public Entity(String name) {
		this.name = name;
	}
	
	public void init(World world) {
		this.world = world;
	}
	
	public void remove() {
		removed = true;
	}
	
	public boolean intersects(int x1, int y1, int w, int h) {
		return !(x + width < x1 || y + height < y1 || x - width > x1 + w || y - height > y1 + h);
	}
	
	public void tick() {
		
	}
	
	public void move(int vx, int vy) {
		if (vx != 0 && vy != 0)
			return;
		
		ArrayList<Entity> isInside = new ArrayList<Entity>();

		isInside = world.getEntities(rect);
		for (Entity e : isInside) {
			if (e.touchedBy(this))
				return;
		}
		
		x += vx;
		y += vy;
		rect.setLocation(x, y);
	}
	
	public void render(Screen screen) {
		
	}
	
	public boolean isInside(Entity e) {
		return false;
	}
	
	public boolean touchedBy(Entity entity) {
		return false;
	}
	
	public void reverse() {

	}

	public void drop() {

	}

}
